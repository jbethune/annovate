
//! Library for lightweight and flexible file annotations
//! 
//! Annovate is designed for researchers who produce a lot of files with a
//! special focus on Bioinformatics. While doing research we tend to try
//! different parameters and different thresholds. In addition, we use filters
//! and transformations on our data. This often leads to file names that look
//! like "TLX343.hg38.min_read19.filtered.NoSingletons" that are hard to
//! understand again a few months in the future.
//!
//!
//! Typical usage:
//!
//! ```
//! //this is a helper function for this example
//! fn get_current_time_str() -> String {
//!     extern crate time;
//!     let now = time::now();
//!     format!("{}.{}.{} {}:{}:{}",
//!              now.tm_mday,
//!              now.tm_mon + 1,
//!              now.tm_year,
//!              now.tm_hour,
//!              now.tm_min,
//!              now.tm_sec )
//! }
//!
//! //another helper function for this example
//! fn get_computer_name() -> String {
//!     use std::process::Command;
//!     let output = Command::new( "hostname" )
//!         .output()
//!         .expect( "Failed to get hostname" );
//!     String::from_utf8( output.stdout ).unwrap()
//! }
//!
//! extern crate libannovate;
//! let mut anno = libannovate::Annovations::open( "example_data/test_cases/.annovate" ).unwrap();
//! // ...
//! // do some work and produce a result file (let's call the file foo.txt)
//! // ...
//! //
//! let context = format!( "{} on {}", get_current_time_str(), get_computer_name() ); //set up context that documents in which situation the file has been created
//! anno.add( "foo.txt", "description", "reads from t17 drosophila mutant strain", &context ); //give a human-readable description
//! anno.add( "foo.txt", "min-coverage", "4", &context ); //document parameter
//! anno.add( "foo.txt", "qc-threshold", "70", &context); //document another parameter
//! anno.add( "foo.txt", "origin", "/home/alice/bin/aligner.exe --min-coverage 4 --qc-threshold 70 drosophila.fasta > foo.txt", &context ); //document the creation procedure. Also handy for reminding yourself
//! //anno.save(); //store annotations to disk
//! ```
//!
//! Retrieving information:
//!
//! ```
//! extern crate libannovate;
//! let anno = libannovate::Annovations::open( "example_data/test_cases/.annovate" ).unwrap();
//! let simple_annovate_file_description = &anno.get( "simple.anno", "description" ).unwrap().value; //get the latest QC threshold
//! ```
//!
//! Annovate does not overwrite information. Because sometimes we actually need
//! to know what a value used to be. To get all annotations from all times, use
//! the .get_history() method
//!
//! ```
//! extern crate libannovate;
//! let anno = libannovate::Annovations::open( "example_data/test_cases/.annovate" ).unwrap();
//! let all_qc_values = anno.get_history( "foo.txt", "qc-threshold" );
//! ```
//!

/*
 * TODO thoughtline for documentation:
 *
 * 1. Rationale -> link to different document
 * 2. Common Usage
 * 3. The file format
 */

/*
 * TODO integrate this into the documentation
 * API:
 *
 x anno.open( annovation_filename ) -> anno
 x anno.add( filename, key, value, context ) -> void; appends an annovation to the list
 x anno.get( filename, key ) -> option(record); get the latest single value
 x anno.get_history( filename, key ) -> [all records for the filename and key]
 *
 x anno.get_filenames() -> [filenames]
 x anno.get_records( filename ) -> [records]
 x anno.get_keys( filename ) -> {all keys available for filename}
 *
 x anno.prune( filename, key ) -> int; returns the number of entries removed
 x anno.save()
 x anno.save_as( path )
 *
 * convenience:
 x to_hash_map( filename ) -> latest key->record pairings
 *
 */

extern crate failure;

use std::path::{Path,PathBuf};
use std::fs::File;
use std::io::{BufRead, BufReader, Write, BufWriter};
use std::collections::{HashMap,HashSet};

#[macro_use] extern crate failure_derive;

use failure::{Error};

#[derive(Debug,Fail)]
#[fail( display = "Syntax error in line {}. With error: {}", line_no, message )]
pub struct AnnovateParserError {
    line_no: usize,
    message: String
}

/// The central annotation atom. Every annotation consists of 3 parts:
/// A key, a value and a context. The context describes the point in time and
/// the circumstances under which the key-value pair was inserted. In general
/// the date and the program that created the annotation should be stored as
/// part of the context.
#[derive(PartialEq, Debug)]
pub struct Record {
    pub key: String,
    pub value: String,
    pub context: String
}

impl Record {
    fn serialize( &self ) -> String {
        let mut result = String::new();
        for line in self.key.lines() {
            result += ">";
            result += line;
            result += "\n";
        }
        for line in self.value.lines() {
            result += "=";
            result += line;
            result += "\n";
        }
        for line in self.context.lines() {
            result += "<";
            result += line;
            result += "\n";
        }
        result += "$";
        result
    }
}

impl Record {
    pub fn new( key: &str, value: &str, context: &str ) -> Record {
        Record{ key: key.to_string(),
                value: value.to_string(),
                context: context.to_string() }
    }
}

/*
#   First line of file. Contains annovate file format version.
$   terminate and push record
@   define new named entry
>   define key line
=   define value line
<   define cntext line

The line "@" means assign annotations to the directory itself.
The default annotation target is the directory itself.

*/

#[derive(Debug)]
pub struct Annovations {
    annovation_file_path: PathBuf,
    records: HashMap<String,Vec<Record>> //filename -> [Record]
}

impl Annovations {
    pub fn open<P: AsRef<Path>>( input_file: P ) -> Result<Annovations, Error> {
        let mut path = PathBuf::new();
        path.push( &input_file ); //I wonder if these is a more elegant way
        if input_file.as_ref().is_file() { //make sure file exists and is a regular file
            Ok( Annovations { annovation_file_path: path,
                              records: read_annovate_records_from_file( input_file )? } )
        } else { //return an empty HashMap as a default for a missing annotations file.
            Ok( Annovations { annovation_file_path: path,
                              records: HashMap::new() } )
        }
    }


    /// Get all records (past and present) for a file
    pub fn get_records( &self, filename: &str ) -> Option<&Vec<Record>> {
        self.records.get( filename )
    }

    pub fn to_hash_map( &self, filename: &str ) -> Option<HashMap<&str,&Record>> {
        let mut result = HashMap::new();
        if let Some( records ) = self.get_records( filename ) {
            for record in records {
                result.insert( &*record.key, &*record );
            }
            Some( result )
        } else {
            None
        }
    }

    pub fn add( &mut self, filename: &str, key: &str, value: &str, context: &str ) {
        self.records.entry( filename.to_string() ).or_insert_with( Vec::new )
            .push( Record::new( key, value, context ) );
    }

    pub fn get_history<'a>( &'a self, filename: &str, key: &str ) -> Vec<&'a Record> {
        if let Some( file_annotations ) = self.records.get( filename ) {
            let mut result = Vec::new();
            for record in file_annotations {
                if record.key == key {
                    result.push( record )
                }
            }
            result
        } else {
            Vec::new() //file name is not in the annotations
        }
    }

    pub fn get<'a>( &'a self, filename: &str, key: &str ) -> Option<&'a Record> {
        if let Some( ref_entry ) = self.get_history( filename, key ).last() {
            Some( ref_entry )
        } else {
            None
        }
    }

    pub fn prune( &mut self, filename: &str, key: &str ) {
        let mut indices_of_elements_to_remove = Vec::new();
        if let Some( records ) = self.records.get( filename ) {
            for ( idx, record ) in records.iter().enumerate() {
                if record.key == key {
                    indices_of_elements_to_remove.push( idx )
                }
            }
        } else {
            return;
        }

        let records = self.records.get_mut( filename ).unwrap(); //get mutable version for mutation
        while let Some( idx ) = indices_of_elements_to_remove.pop() { //remove indices in reverse order to keep indices correct even after shifts
            records.remove( idx );
        }

    }


    pub fn save( &self ) -> Result<(), Error> {
        self.save_as( &self.annovation_file_path.as_path() )
    }

    pub fn save_as<P: AsRef<Path>>( &self, output_file: P ) -> Result<(), Error> {
        let fd = File::create( output_file )?;
        let mut writer = BufWriter::new( fd );
        writer.write_all( b"#annovate 1.0\n" )?;

        //first take care of the directory itself so that it is printed first (even though this is
        //not a strict requirement)
        if let Some( records ) = self.records.get( "" ) {
            for record in records {
                writer.write_all( record.serialize().as_bytes() )?;
            }
        }

        //then iterate over all other entries
        for ( filename, file_records ) in &self.records {
            if filename == "" {
                continue; //already taken care of
            }
            for record in file_records {
                writer.write_all( format!( "@{}\n", filename ).as_bytes() )?;
                writer.write_all( record.serialize().as_bytes() )?;
            }
        }

        Ok( () )

    }

    pub fn get_keys<'a>( &'a self, filename: &str ) -> HashSet<&'a str> {
        let mut result = HashSet::new();
        if let Some( records ) = self.records.get( filename ) {
            for record in records {
                result.insert( record.key.as_str() );
            }
        }
        result
    }

    pub fn get_filenames<'a>( &'a self ) -> Vec<&'a str> {
        let mut result = Vec::new();
        for filename in self.records.keys() {
            result.push( filename.as_str() );
        }
        result
    }
}

fn read_annovate_records_from_file<P: AsRef<Path>>( input_file: P ) -> Result<HashMap<String,Vec<Record>>, Error> {
    let file = File::open( input_file )?;
    let reader = BufReader::new( file );

    // the final result of this function
    let mut annotations = HashMap::new(); //file -> [(key,value,context)]

    let mut current_file_name = String::new(); //empty string: The directory itself

    /* accumulator string variables that will extend until the last line for the
     * key/value/context has been read */
    let mut current_key = String::new();
    let mut current_value = String::new();
    let mut current_context = String::new();

    fn append_multiline( s: &mut String, additional_content: &str ) {
        if !s.is_empty() {
            *s += "\n";
        }
        *s += additional_content;
    }

    for ( line_no, line_ )  in reader.lines().enumerate() {
        let line = line_?;

        if line_no == 0 { //then check the header
            if line.starts_with( "#annovate " ) {
                continue //input is as expected
            }
            return Err( AnnovateParserError{ line_no: line_no + 1, message:  "First line must start with #annovate".to_string() } )?;
        }

        if let Some( prefix ) = line.chars().next() {
            let content = line[ 1.. ].to_string();
            match prefix.to_string().as_ref() {
                "$" => { //assemble record and clear buffers
                    let records = annotations.entry( current_file_name.clone() ).or_insert_with( Vec::new );
                    records.push( Record::new( &current_key, &current_value, &current_context ) );
                    current_key = String::new();
                    current_value = String::new();
                    current_context = String::new();
                },
                "@" => current_file_name = content.clone(), //TODO reconsider how avoid clone
                ">" => append_multiline( &mut current_key, &content ),
                "=" => append_multiline( &mut current_value, &content ),
                "<" => append_multiline( &mut current_context, &content ),
                _ =>  return Err( AnnovateParserError{ line_no: line_no + 1, message: format!( "Invalid prefix {}", prefix ).to_string() } )?
            };
        } else {
            return Err( AnnovateParserError{ line_no: line_no + 1, message: "Line is empty".to_string() } )?
        }

    }
    Ok( annotations )
}


#[cfg(test)]
mod tests {
    use super::*;

    fn slurp( filename: &str ) -> String {
        use std::io::Read;
        let mut file = File::open( filename ).unwrap();
        let mut contents = Vec::new();
        file.read_to_end( &mut contents ).unwrap();
        String::from_utf8( contents ).unwrap()
    }

    #[test]
    fn test_read_annovate_records_from_file() {
        let example = Annovations::open( "example_data/test_cases/simple.anno" ).unwrap();
        let dir_annotations = example.get_records( "" ).unwrap();
        let file_annotations = example.get_records( "first-file" ).unwrap();
        assert_eq!( dir_annotations[ 0 ], Record::new( "example key",
                                                       "example value" ,
                                                       "example context" ) );
        assert_eq!( dir_annotations[ 1 ], Record::new( "another example key",
                                                       "another example value" ,
                                                       "another example context" ) );
        assert_eq!( file_annotations[ 0 ], Record::new( "file-key",
                                                        "file-value" ,
                                                        "file-context" ) );
    }

    #[test]
    fn test_to_hash_map() {
        let example = Annovations::open( "example_data/test_cases/overriding_keys.anno" ).unwrap();
        let dir_annotations = example.to_hash_map( "" ).unwrap();
        let file_annotations = example.to_hash_map( "foo-file" ).unwrap();
        assert_eq!( dir_annotations.get( "key1" ).unwrap().value, "value1b" );
        assert_eq!( file_annotations.get( "key1" ).unwrap().value, "value1B" );
    }

    #[test]
    fn test_non_existing_file() {
        let example = Annovations::open( "does-not-exist" ).unwrap();
        assert_eq!( example.records.len(), 0 );
        assert_eq!( example.annovation_file_path.as_path(), Path::new( "does-not-exist" ) );
    }

    #[test]
    fn test_add() {
        let mut example = Annovations::open( "example_data/test_cases/simple.anno" ).unwrap();
        example.add( "first-file", "file-key", "overriding value", "testing" );
        let latest = example.to_hash_map( "first-file" ).unwrap();
        assert_eq!( latest.get( "file-key" ),
                    Some( &&Record::new( "file-key",
                                         "overriding value",
                                         "testing" ) )
        );
        let file_annotations = example.get_records( "first-file" ).unwrap();
        assert_eq!( file_annotations[ 0 ], Record::new( "file-key",
                                                        "file-value" ,
                                                        "file-context" ) );
    }

    #[test]
    fn test_parsing_of_multiline_entries() {
        let example = Annovations::open( "example_data/test_cases/multiline_fields.anno" ).unwrap();
        let entries = example.to_hash_map( "filenames-are-always-single-lines" ).unwrap();
        assert_eq!( entries.get( "key 1\nwith more than\none line" ).unwrap().value, "equally there is also\na value with more than one line" );
        assert_eq!( entries.get( "key 2\nhas also more than one line" ).unwrap().value, "When it comes to multiline entries\nI think that values will be the most common\nuse case" );
    }

    #[test]
    fn test_parse_failures() {
        let example = Annovations::open( "example_data/test_cases/parse_error.anno" );
        assert!( example.is_err() );
    }

    #[test]
    fn test_prunes() {
        let mut example = Annovations::open( "example_data/test_cases/overriding_keys.anno" ).unwrap();
        example.prune( "foo-file", "key1" );
        let annotations = example.to_hash_map( "foo-file" );
        assert_eq!( annotations.unwrap().get( "key1" ), None );
        let annotations = example.to_hash_map( "" );
        assert_eq!( annotations.unwrap().get( "key1" ).unwrap(),
                    &&Record::new( "key1", "value1b", "context1b" ) );

        let mut example = Annovations::open( "example_data/test_cases/overriding_keys.anno" ).unwrap();
        example.prune( "foo-file", "key-does-not-exist" );
        let annotations = example.to_hash_map( "foo-file" );
        assert_eq!( annotations.unwrap().get( "key-does-not-exist" ), None );
    }

    #[test]
    fn test_serialization_of_records() { //this test is long, because I want to do things in a very simple rather than efficient way to ensure correctness
        fn compare_serialization_with_original_data( annovate_file: &str ) {
            let text = slurp( annovate_file );
            let mut file_ordering = Vec::new();
            file_ordering.push( String::new() ); //for the directory itself
            for line in text.lines() {
                if line.starts_with( "@" ) {
                    file_ordering.push( String::from( &line[ 1.. ] ) );
                }
            }

            let mut serialized_text = String::new();
            serialized_text += "#annovate 1.0\n";
            let annotations = Annovations::open( annovate_file ).unwrap();
            for filename in file_ordering {
                if filename != "" {
                    serialized_text += &format!( "@{}\n", filename );
                }

                for record in annotations.records.get( &filename ).unwrap_or( &Vec::new() ) { //unwrap_or() because the directory itself `""` might not have a vector of records because there are no key-value-contexts triplets for the directory itself
                    serialized_text += &record.serialize();
                    serialized_text += "\n";
                }
            }

            for ( file_line, serialized_line ) in text.lines().zip( serialized_text.lines() ) {
                assert_eq!( file_line, serialized_line );
            }
        }

        compare_serialization_with_original_data( "example_data/test_cases/simple.anno" );
        compare_serialization_with_original_data( "example_data/test_cases/overriding_keys.anno" );
        compare_serialization_with_original_data( "example_data/test_cases/multiline_fields.anno" );
    }

    #[test]
    fn test_missing_header() {
        let annotations = Annovations::open( "example_data/test_cases/missing_header.anno" );
        assert!( annotations.is_err() );
        let err = annotations.unwrap_err();
        if let Some( actual_err ) = err.downcast_ref::<AnnovateParserError>() {
            assert_eq!( actual_err.line_no, 1 );
        } else {
            assert!( false ); //incorrect error type
        }
    }

    #[test]
    fn test_get_history() {
        let example = Annovations::open( "example_data/test_cases/overriding_keys.anno" ).unwrap();
        let records = example.get_history( "foo-file", "key1" );
        assert_eq!( records.len(), 2 );
        assert_eq!( records[ 0 ].key, "key1" );
        assert_eq!( records[ 1 ].key, "key1" );
        assert_eq!( records[ 0 ].value, "value1A" );
        assert_eq!( records[ 1 ].value, "value1B" );
        assert_eq!( records[ 0 ].context, "context1A" );
        assert_eq!( records[ 1 ].context, "context1B" );
        let no_records = example.get_history( "foo-file", "key does not exist" );
        assert_eq!( no_records.len(), 0 );
        let not_annotated_file = example.get_history( "not annotated file", "whatever" );
        assert_eq!( not_annotated_file.len(), 0 );
    }

    #[test]
    fn test_get() {
        let example = Annovations::open( "example_data/test_cases/overriding_keys.anno" ).unwrap();
        let record = example.get( "foo-file", "key1" ).unwrap();
        assert_eq!( record.key, "key1" );
        assert_eq!( record.value, "value1B" );
        assert_eq!( record.context, "context1B" );

        let no_record = example.get( "foo-file", "key does not exist" );
        assert!( no_record.is_none() );

        let not_annotated_file = example.get( "not annotated file", "whatever" );
        assert!( not_annotated_file.is_none() );
    }

    #[test]
    fn test_get_keys() {
        let example = Annovations::open( "example_data/test_cases/simple.anno" ).unwrap();
        let mut sorted_keys = Vec::new(); //there is probably a more elegant way
        sorted_keys.extend( example.get_keys( "" ) );
        sorted_keys.sort_unstable();

        assert_eq!( sorted_keys.len(), 2 );
        assert_eq!( sorted_keys[ 0 ], "another example key" );
        assert_eq!( sorted_keys[ 1 ], "example key" );
    }

    #[test]
    fn test_get_filenames() {
        let example = Annovations::open( "example_data/test_cases/simple.anno" ).unwrap();
        let mut sorted_filenames = example.get_filenames();
        sorted_filenames.sort_unstable();
        assert_eq!( sorted_filenames[ 0 ], "" );
        assert_eq!( sorted_filenames[ 1 ], "first-file" );
        assert_eq!( sorted_filenames[ 2 ], "second-file" );
    }
}

//TODO implement retrieval for file paths. That means open annovate files at the same location as
//the files even if annovate is executed in a different directory
//I think there should be a helper class for this that is potentially more expensive to use than
//direct retrieval of a directory's contents
