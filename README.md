# Annovate

Annovate is a software library to manage scientific metadata. The purpose of
this library is to automatically annotate files that have been produced by
scientific workflows. A common problem is that scientists try many different
parameters to produce computational results. If these parameters are not
properly documented, scientific results become irreproducible. Annovate tries
to provide a convenient way of automatically adding annotations to your result
files so that you will always have the latest parameters and commands to
recreate your results.

## Current state of the software

Right now the main library is in good shape. Next up are programs that will use
this library and that will provide command-line and GUI interfaces that focus
on ergonomic usage for both interactive and scripted usage.